var myLati = 50;
var myLong = 10;
var myAlti = 100;

function doAction() {
  getLocation();
//  getAirData();
//  getThermData();
  getAllSensors();
}

function getHomeSensor() {
  getAirData();
  getThermData();
}
//
// Alerting the Info-Block
function getInfo() {
  alert('The webApp shows currrent air pollution data, temperature and huminity data. \n' +
          'Gathered by: \n' +
         'https://luftdaten.info' );
}
//
// Alerting the Help-Block
function getHelp() {
  alert('No help info available. \n'); 
}


// Useful webpages
// ..

function getAllSensors() {
  var p1Sum = 0.0; p1Weight = 0.0; i=0; j=0; 
  var p2Sum = 0.0; p2Weight = 0.0; t1Sum = 0; t1Weight = 0; h1Sum = 0; h1Weight = 0;
  var d1Sum = 0; d1Weight = 0; lWeight = 0;

  txtout("Loading..","c_avgP1"); 
  txtout("Loading..","c_avgP2"); 
  txtout("Loading..","c_avgT1"); 
  txtout("Loading..","c_avgH1"); 
  txtout("Loading..","c_avgD1");    
  txtout("Loading..","c_altvalue");  

  fetch("https://data.sensor.community/static/v1/data.json")
//  fetch ("https://api.luftdaten.info/static/v1/data.json")
.then(
  function(response) {

    if (response.status !== 200) {
      console.log('Looks like there was a problem. Status Code: ' +
        response.status);
      return;
    }
    // Examine the text in the response
    response.json().then(function(data) {
      console.log(data[100]);   
      var gdiff = sqrt(360*360+180*180); mdiff  = gdiff;
      for (i = 0; i < data.length; i++) {
        lati = data[i].location.latitude;
        long = data[i].location.longitude;
        gdiff = cos(myLati/180*3.1415926)*sqrt(power((lati-myLati),2)+power((long-myLong),2))
       if (gdiff <= mdiff) {
            alti = data[i].location.altitude;
            mdiff = gdiff;
            dist = gdiff*111.1+0.01;
            lWeight = 1/dist;
            nextId = i;
            console.log(lati,long,myLati,myLong,nextId);
            console.log(data[i]);
            for (j = 0; j < data[i].sensordatavalues.length; j++) {
              console.log(data[i].sensordatavalues[j]);
              if (data[i].sensordatavalues[j].value_type == "P1") {
                p1Weight = p1Weight + lWeight;
                p1Sum = p1Sum + lWeight*data[i].sensordatavalues[j].value;
                console.log(dist, p1Weight, p1Sum/p1Weight)
                p1value = data[i].sensordatavalues[j].value
                txtout(form(p1value,0)+"  µg/m^3","c_p1value");
                document.getElementById("p1Meter").setAttribute("value", 255-p1value);                
              }
              if (data[i].sensordatavalues[j].value_type == "P2") {
                p2Weight = p2Weight + lWeight;
                p2Sum = p2Sum + lWeight*data[i].sensordatavalues[j].value;
                console.log(dist, p2Weight, p2Sum/p2Weight)
                p2value = data[i].sensordatavalues[j].value
                txtout(form(p2value,0)+"  µg/m^3","c_p2value");  
                document.getElementById("p2Meter").setAttribute("value", 55-p2value);                
              }        
              if (data[i].sensordatavalues[j].value_type == "temperature") {
                t1Weight = t1Weight + lWeight;
                t1Sum = t1Sum + lWeight*data[i].sensordatavalues[j].value;
                console.log(dist, t1Weight, t1Sum/t1Weight)
                t1value = data[i].sensordatavalues[j].value
                txtout(form(t1value,0)+"  °C","c_t1value");
                document.getElementById("t1Meter").setAttribute("value", t1value);
              }      
              if (data[i].sensordatavalues[j].value_type == "humidity") {
                h1Weight = h1Weight + lWeight;
                h1Sum = h1Sum + lWeight*data[i].sensordatavalues[j].value;
                console.log(dist, h1Weight, h1Sum/h1Weight)
                h1value = data[i].sensordatavalues[j].value
                txtout(form(h1value,0)+"  %","c_h1value"); 
                document.getElementById("h1Meter").setAttribute("value", h1value);
              }   
              if (data[i].sensordatavalues[j].value_type == "pressure") {
                d1Weight = d1Weight + lWeight;
                d1Sum = d1Sum + lWeight*data[i].sensordatavalues[j].value;
                console.log(dist, d1Weight, d1Sum/d1Weight)
              }                                                
            }

        }
      
      }
      if (d1Weight == 0) {
        d1Sum = 101300; d1Weight = 1;
      }
    
      var avgP1 = p1Sum/p1Weight;
      var avgP2 = p2Sum/p2Weight;
      var avgT1 = t1Sum/t1Weight;
      var avgD1 = d1Sum/d1Weight/100;
      var avgH1 = h1Sum/h1Weight;
      txtout(form(avgP1,0)+"  µg/m^3","c_avgP1"); 
      txtout(form(avgP2,0)+"  µg/m^3","c_avgP2"); 
      txtout(form(avgT1,0)+"  °C","c_avgT1"); 
      txtout(form(avgH1,0)+"  %","c_avgH1"); 
      txtout(form(avgD1,0)+"  mBar","c_avgD1");    
      txtout(form(alti,0)+"  m","c_altvalue");      

      document.getElementById("distance").innerHTML = "Nearest Sensor is in " + form(dist,3) + " km";
      var d = new Date();
      document.getElementById("timestamp").innerHTML = d;
    
    });
  })

}


function getAirData() {

fetch("https://data.sensor.community/airrohr/v1/sensor/23230/")
.then(
  function(response) {
    console.log(response.status)
    if (response.status !== 200) {
      console.log('Looks like there was a problem. Status Code: ' +
        response.status);
      return;
    }
    // Examine the text in the response
    response.json().then(function(data) {
      console.log(data);
      p1value = data[0].sensordatavalues[0].value
      txtout(form(p1value,0)+"  µg/m^3","c_p1value");
      p2value = data[0].sensordatavalues[1].value
      txtout(form(p2value,0)+"  µg/m^3","c_p2value");      
      d1value =  100*(p1value - data[1].sensordatavalues[0].value)/(p1value);
      txtout(form(d1value,0)+"  %","c_d1value");
      d2value =  100*(p2value - data[1].sensordatavalues[1].value)/(p2value);
      txtout(form(d2value,0)+"  %","c_d2value");        
    });
  }
)
.catch(err => {
	console.log(err);
});

}


function getThermData() {  
  fetch("https://data.sensor.community/airrohr/v1/sensor/23231/")
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }
      // Examine the text in the response
      response.json().then(function(data) {
        console.log(data);
        t1value = data[0].sensordatavalues[0].value
        txtout(form(t1value,0)+"  °C","c_t1value");
        h1value = data[0].sensordatavalues[1].value
        txtout(form(h1value,0)+"  %","c_h1value");      
        dt1value =  100*(t1value - data[1].sensordatavalues[0].value)/(t1value);
        txtout(form(dt1value,0)+"  %","c_dt1value");
        dh1value =  100*(h1value - data[1].sensordatavalues[1].value)/(h1value);
        txtout(form(dh1value,0)+"  %","c_dh1value");        
      });
    }
  )
  .catch(err => {
    console.log(err);
  });
  
  }


  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      alert('Geolocation is not supported by this browser.');
    }
  }

  function showPosition(position) {
    myLati = position.coords.latitude;
    myLong = position.coords.longitude;
    txtout(form(myLati ,3)+"  °","c_latvalue"); 
    txtout(form(myLong ,3)+"  °","c_lonvalue"); 
    console.log(position);

    vmbox = myLati - 0.001;
    vpbox = myLati + 0.001;
    hmbox = myLong - 0.001;
    hpbox = myLong + 0.001;
    locurl = "https://www.openstreetmap.org/export/embed.html?bbox="+hmbox+"%2C"+vmbox+"%2C"+hpbox+"%2C"+vpbox+"&amp;layer=hot"
    document.getElementById("mapholder").src = locurl;
  
    console.log(locurl);
  }

//
// Formatting Number
function form(x, z) {
  var y;
  x = Math.round(x * power(10, z));
  y = x / power(10, z);
  return y;
}

//
// Formatting text output
function txtout(txtstr,elemstr) {
var list = document.getElementById(elemstr);
while (list.hasChildNodes()) {
  list.removeChild(list.firstChild);
}

var wrtline = document.createTextNode(txtstr);
var element = document.getElementById(elemstr).appendChild(wrtline);
}
//
// Power-Function
function power(x, z) {
  var y;
  y = Math.pow(x, z);
  return y;
}
//
// Root-Function
function sqrt(x) {
  var y;
  y = Math.sqrt(x);
  return y;
}

//
// Cos-Function in Degrees
function cos(x) {
  var y;
  y = Math.cos(x * Math.PI / 180);
  return y;
}
